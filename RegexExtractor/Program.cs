﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegexExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            //ToDelete();
            if (args.Length < 2)
                PrintUsage();
            
            else
            {
                CompareRegex cr = new CompareRegex(args[1]);

                string text = "";
                string result = "";
                if (File.Exists(args[0]))
                {
                    int i = 0;
                    FileActions fa = new FileActions(args[0]);

                    while (!FileActions.EverythingRead)
                    {
                        text = fa.ReadFromFile(i);
                        result = cr.Compare(text);
                        FileActions.WriteToFile(result, i);

                        i++;
                    }
                }
                else
                {
                    text = args[0];
                    result = cr.Compare(text);
                    FileActions.WriteToFile(result, 0);
                }

            }
            
            Console.ReadLine();
        }

        static void PrintUsage()
        {
            foreach(string text in Values.UsageText())
                Console.WriteLine(text);
        }

        //method to create a large file quickly
        static void ToDelete()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10000000; i++)
                sb.Append(i);
            File.AppendAllText("big.txt", sb.ToString());
        }
    }
}
