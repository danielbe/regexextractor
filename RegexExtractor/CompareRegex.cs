﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegexExtractor
{
    class CompareRegex
    {
        readonly string pattern;

        internal CompareRegex(string pattern)
        {
            this.pattern = pattern;
        }

        internal string Compare(string against)
        {
            StringBuilder output = new StringBuilder();

            Regex regex = new Regex(pattern);
            MatchCollection matches = regex.Matches(against);

            foreach (Match match in matches)
            {
                output.Append(match + Environment.NewLine);
            }

            //Console.WriteLine(output.ToString());
            Console.WriteLine(Values.NrOfFoundFiles(matches.Count));

            return output.ToString();
        }


    }
}
