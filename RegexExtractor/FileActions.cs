﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegexExtractor
{
    class FileActions
    {
        internal static bool EverythingRead = false;
        private static readonly int stopAtChar = 10000000;
        private readonly string fileLocation;

        internal FileActions(string fileLocation)
        {
            this.fileLocation = fileLocation;
        }

        internal string ReadFromFile(int iteration)
        {
            string ret = "";
            FileInfo info = new FileInfo(fileLocation);

            if (info.Length > stopAtChar)
            {
                byte[] tempBytesOfFile = new byte[stopAtChar];

                using (FileStream stream = new FileStream(fileLocation, FileMode.OpenOrCreate))
                {
                    //Set read start position of file
                    stream.Seek(stopAtChar * iteration, SeekOrigin.Begin);
                    stream.Read(tempBytesOfFile, 0, stopAtChar);

                    if (stream.Position >= info.Length)
                        EverythingRead = true;
                }
                StringBuilder fileContent = new StringBuilder();
                foreach (char b in tempBytesOfFile)
                {
                    if (!b.Equals('\0'))
                        fileContent.Append(b);
                }
                ret = fileContent.ToString();
            }
            else
            {
                ret = File.ReadAllText(fileLocation);
                EverythingRead = true;
            }

            return ret;
        }

        internal static void WriteToFile(string text, int iteration)
        {
            string fileName = Values.OutputFileName(iteration);
            File.WriteAllText(fileName, text);
        }
    }
}
