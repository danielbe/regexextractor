﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegexExtractor
{
    internal static class Values
    {
        //currently unused
        internal static StringBuilder OutputText = new StringBuilder();

        internal static string OutputFileName(int iteration)
        {
            return $"foundMatches_{iteration}.txt";
        }

        #region Text
        public static string NrOfFoundFiles(int number)
        {
            return $"{number} found matches were written in foundMatches.txt file";
        }

        public static List<string> UsageText()
        {
            return new List<string>()
            {
                "Usage: RegexExtractor.exe FileOrText Regex",
                "RegexExtractor.exe text.txt [a-zA-Z]+23",
                "A new file will be created containing the matches",
                "",
                "To divide a big file into smaller pieces type",
                "RegexExtractor.exe text.txt .*"
            };
        }
        #endregion
    }
}
